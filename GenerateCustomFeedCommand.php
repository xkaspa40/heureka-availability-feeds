<?php
/*
 * This file is part of oXyShop PreLucid 0.7
 *
 * (c) oXyShop <info@oxyshop.cz>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Oxyshop\Lucid\App\CustomXMLFeeds\Command;

use Oxyshop\CustomXmlFeeds\Adapter\Oxid\Data\HeurekaCzRawDataProvider;
use Oxyshop\CustomXmlFeeds\Adapter\Oxid\Data\HeurekaRawDataProvider;
use Oxyshop\CustomXmlFeeds\Command\GenerateFeedCommand;
use Oxyshop\CustomXmlFeeds\Interpreter\HeurekaCzInterpreter;
use Oxyshop\Lucid\App\CustomXMLFeeds\Manager\CustomHeurekaAvailabilityManager;
use Oxyshop\Lucid\App\CustomXMLFeeds\Manager\CustomHeurekaManager;

class GenerateCustomFeedCommand extends GenerateFeedCommand
{
    /** @var string */
    const TRANSFORMATION_DIR = __DIR__.'/../Manager/transformations/';
    /**
     * @var array
     */
    public static $feeds = [];

    /**
     * @return array
     */
    protected function getCustomConfig()
    {
        return [
            CustomHeurekaManager::CUSTOM_FEEDNAME => [
                'manager' => CustomHeurekaManager::class,
                'dataProvider' => HeurekaCzRawDataProvider::class,
                'intepreter' => HeurekaCzInterpreter::class,
                'language' => '_2',
                'currency' => 'CZK',
            ],
            CustomHeurekaAvailabilityManager::CUSTOM_FEEDNAME => [
                'manager' => CustomHeurekaAvailabilityManager::class,
                'dataProvider' => HeurekaRawDataProvider::class,
                'intepreter' => HeurekaCzInterpreter::class,
                'language' => '_2',
                'currency' => 'CZK',
                'transformationDir' => self::TRANSFORMATION_DIR,
            ],
        ];
    }
}
