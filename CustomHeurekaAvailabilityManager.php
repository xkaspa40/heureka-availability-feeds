<?php
/*
 * This file is part of oXyShop PreLucid 0.7
 *
 * (c) oXyShop <info@oxyshop.cz>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Oxyshop\Lucid\App\CustomXMLFeeds\Manager;

use Oxyshop\CustomXmlFeeds\Manager\HeurekaManager;

class CustomHeurekaAvailabilityManager extends HeurekaManager
{
    /** @var string */
    const CUSTOM_FEEDNAME = 'heureka_availability';

    /** @var string */
    protected $transformationName = 'custom_heureka_availability.xml';

    protected $aliases = ['cz_heureka'];

    /** @var bool */
    protected $useZip = false;

    /**
     * @return string
     */
    public function getFeedname(): string
    {
        return self::CUSTOM_FEEDNAME;
    }

    /**
     * @return array
     */
    protected function prepareFeedData(): array
    {
        $data = parent::prepareFeedData();

        if (empty($data)) {
            return [];
        }
        $data = $this->manageQuantity($data);

        return $data;
    }

    /**
     * Manages quantity and removes unavailable articles.
     *
     * @param array $data
     *
     * @return array
     */
    protected function manageQuantity(array $data): array
    {
        $stock = $this->getStock($data['id']);

        if ($stock <= 0) {
            return [];
        }
        $data['quantity'] = $stock;

        return $data;
    }

    /**
     * Gets OXSTOCK based on article id.
     *
     * @param string $id
     *
     * @return int
     *
     * @throws \oxConnectionException
     */
    protected function getStock(string $id): int
    {
        $db = \oxDb::getDb(true);
        $stock = $db->getOne('SELECT `OXSTOCK` FROM `oxarticles` WHERE `OXID` = ?', $id);

        return null === $stock ? 0 : $stock;
    }
}
